---
title: Self-organizing map
date: 2018-04-16
tags: ["Neurónové siete"]
---

## Vektorová kvantizácia

Vektorová kvantizácia je známa z oblasti spracovania digitálneho signálu, no kedže je založená na učení súťažením (competitive learning), tak úzko súvisí 
so SOM-kami. Podstata je, že priestor n-rozmerných vstupných dát (vektorov) je rozdelená do konečného počtu spojitých oblasti, kde jednotlivé oblsati sú 
reprezentované optimálne jedním vektorom. 

Majme teda množinu n-rozmerných vstupov a konečnú množinu d-rozmerných kvantizačných vektorov $`M`$ . Potom víťaz (pozn. competitive learnig) $`c`$ sa nazýva ten $`m_c \in M`$, pre ktorý
platí:



$```c = argmin_{i} {|| x - m_i ||}```$


Potom jednoduchý algoritmus VQ:
   
    * Pre náhodný vstup $`x`$ nájdi najbližší kvantizačný vektor (winner) $`m`$ 
    * Posuň $`m`$ smerom ku $`x`$ o malú časť vzdialenosti medzi nimi
    * Opakuj kým to má zmysel :D

V optimálnom prípade by malo, potom platiť, že kvantizačné vektory sú konštruované, tak že priemerná vzdialenosť medzi ľubovoľným vstupom $`x`$ a najlepším 
kvantizačným vektorom, pre daný vstup $`x`$, je minimálna, teda aj stredná kvantizačná chyba je minimálna. 

Táto technika sa používa na redukciu dimenzionality, ako klustrovací algoritmus, pri technikách rozpoznávania obrazcov, spracovania zvuku a videa alebo odhad funkcie hustoty pravdepodobnosti.

## SOM 

Doplniť

## Literatúra

* https://pdfs.semanticscholar.org/4e9b/467deb651f1333f4a99331c50760286f452e.pdf
* http://dai.fmph.uniba.sk/courses/NN/Lectures/nn.som.L07.4x.pdf
* https://en.wikipedia.org/wiki/Self-organizing_map
* https://en.wikipedia.org/wiki/Vector_quantization
* https://en.wikipedia.org/wiki/Quantization_(signal_processing)
* https://machinelearningmastery.com/learning-vector-quantization-for-machine-learning/
* https://en.wikipedia.org/wiki/Learning_vector_quantization


